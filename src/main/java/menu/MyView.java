package menu;

import epam.com.InvokeClass;
import epam.com.MyOwnAnnotation;
import epam.com.MyOwnAnnotationInt;
import epam.com.TestClass;
import epam.com.UnknownFieldClass;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MyView {


  private Map<String, String> menu;
  private Map<String, Printable> methodsMenu;
  private static Scanner input = new Scanner(System.in);
  private static Logger logger = LogManager.getLogger();

  public MyView() {

    menu = new LinkedHashMap<>();
    methodsMenu = new LinkedHashMap<>();
    menu.put("1", "1 - Own annotation with annotation value");
    menu.put("2", "2 - Invoke methods with different parameters");
    menu.put("3", "3 - Invoke methods withs String... args");
    menu.put("4", "4 - Class that receive information");
    menu.put("Q", "Quit");

    methodsMenu.put("1", this::firstTask);
    methodsMenu.put("2", this::secondTask);
    methodsMenu.put("3", this::thirdTask);
    methodsMenu.put("4", this::fourthTask);
  }

  private void outputMenu() {
    System.out.println("\n Menu: ");
    for (String str : menu.values()) {
      System.out.println(str);
    }
  }

  private void firstTask() {

    TestClass testClass = new TestClass();
    Class clazz = testClass.getClass();
    Field[] fields = clazz.getDeclaredFields();
    for (Field field : fields) {
      if (field.isAnnotationPresent(MyOwnAnnotation.class)) {
        MyOwnAnnotation myOwnAnnotation;
        myOwnAnnotation = field.getAnnotation(MyOwnAnnotation.class);
        String name = myOwnAnnotation.key();
        System.out.println(field.getName());

        System.out.println("MyOwnAnnotation value: " + name);
      } else if (field.isAnnotationPresent(MyOwnAnnotationInt.class)) {
        MyOwnAnnotationInt myOwnAnnotationInt;
        myOwnAnnotationInt = field.getAnnotation(MyOwnAnnotationInt.class);
        int key = myOwnAnnotationInt.keyInt();
        logger.fatal(field.getName());
        logger.fatal("@myOwnAnnotationInt value: " + key);
      }
    }
  }

  private void secondTask() {
    InvokeClass invokeClass = new InvokeClass();
    testRefl(invokeClass);
  }

  private void testRefl(Object object) {
    try {
      Class clazz = object.getClass();
      Method methodCall = clazz.getDeclaredMethod("publicSum", int.class, int.class);
      methodCall.setAccessible(true);
      logger.fatal(methodCall.invoke(object, 1, 1));

      Method methodCall1 = clazz.getDeclaredMethod("publicSum", double.class, double.class);
      methodCall1.setAccessible(true);
      logger.fatal(methodCall1.invoke(object, 1.0, 1.0));

      Method methodCall2 = clazz.getDeclaredMethod("publicSum", float.class, float.class);
      methodCall2.setAccessible(true);
      logger.fatal(methodCall2.invoke(object, 1.0f, 1.0f));
    } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
      e.printStackTrace();
    }
  }

  private void thirdTask() {
    InvokeClass invokeClass = new InvokeClass();
    testRefl(invokeClass);
  }

  private void testInvoke(Object object) {
    Class clazz = object.getClass();
    String[] strs = {"one", "two", "three"};
    String[] strs3 = {"1", "2", "3"};
    try {
      Method method = clazz
          .getDeclaredMethod("myMethod", new Class[]{String.class, String[].class});
      method.setAccessible(true);
      Method method1 = clazz
          .getDeclaredMethod("myMethod", new Class[]{String[].class});
      method1.setAccessible(true);
      String[] strs2 = (String[]) method.invoke(object, "Number", strs);
      String[] strs4 = (String[]) method1.invoke(object, (Object) strs3);
      for (String s : strs2) {
        logger.fatal(s);
      }

      for (String s : strs4) {
        logger.fatal(s);
      }
    } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
      e.printStackTrace();
    }
  }

  private void fourthTask() {
    UnknownFieldClass<TestClass> obj = new UnknownFieldClass<>(TestClass.class);
  }

  public void show() {
    String keyMenu;
    do {
      outputMenu();
      System.out.println("Please select menu point: ");
      keyMenu = input.nextLine().toUpperCase();
      try {
        methodsMenu.get(keyMenu).print();

      } catch (Exception e) {

      }
    } while (!keyMenu.equals("Q"));
  }


}
