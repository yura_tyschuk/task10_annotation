package epam.com;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class UnknownFieldClass<T> {

  private T obj;
  private final Class<T> clazz;

  public UnknownFieldClass(Class<T> clazz) {
    this.clazz = clazz;

    try {
      clazz.getConstructor().newInstance();
      System.out.println("Class name" + clazz.getSimpleName());
      Field[] fields = clazz.getDeclaredFields();
      for (Field field : fields) {
        System.out.println("Name: " + field.getName());
      }
    } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
      e.printStackTrace();
    }
  }
}

