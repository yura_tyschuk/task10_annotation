package epam.com;

public class TestClass {

  @MyOwnAnnotation
  private String firstKey;
  @MyOwnAnnotation(key = "Second key")
  private String secondKey;

  @MyOwnAnnotationInt
  private int firstInt;
  @MyOwnAnnotationInt(keyInt = 20)
  private int secondInt;


}
