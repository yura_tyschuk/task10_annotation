package epam.com;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class InvokeClass {

  private static Logger logger = LogManager.getLogger();

  private double publicSum(int a, int b) {
    logger.fatal("Integers");
    return a + b;
  }

  private float publicSum(float a, float b) {
    logger.fatal("Float");
    return a + b;
  }

  private double publicSum(double a, double b) {
    logger.fatal("Double");
    return a + b;
  }

  private String[] myMethod(String a, String... args) {
    String[] stringArray = new String[args.length];
    for (int i = 0; i < args.length; i++) {
      stringArray[i] = a + "->" + args[i];
    }
    return stringArray;
  }

  private String[] myMethod(String... args) {
    String[] stringArray = new String[args.length];
    for (int i = 0; i < args.length; i++) {
      stringArray[i] = args[i];
    }
    return stringArray;
  }
}
